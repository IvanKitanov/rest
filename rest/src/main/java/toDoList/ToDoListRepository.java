package toDoList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

interface ToDoRepository extends CrudRepository<ToDoList, Long> {

}