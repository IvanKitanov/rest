package toDoList;

import org.hibernate.annotations.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class ToDoList {
    private String title;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    public ToDoList(){}
    public ToDoList(String title){
        this.title=title;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }
    public void setId(long id){
        this.id=id;
    }

    @Override
    public String toString() {
        return "ToDoList{" +
                "title='" + title + '\'' +
                ", id=" + id +
                '}';
    }
}