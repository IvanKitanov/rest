package toDoList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public class ToDoListService {

    @Autowired
    private ToDoRepository repository;

    List<ToDoList> getAll() {
        List<ToDoList> tasks= (List<ToDoList>) repository.findAll();
        return tasks ;
    }
    ToDoList addNewTask(ToDoList newTask) {
        return repository.save(newTask);
    }
    ToDoList singleTask(long id) {
        return repository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id));
    }
    ToDoList changeTask (ToDoList newTask, Long id) {

        return repository.findById(id).map(task -> {
            task.setTitle(newTask.getTitle());
            return repository.save(task);}).orElseGet(() -> {
            newTask.setId(id);
            return repository.save(newTask);
        });
    }
    void deleteEmployee(Long id) {
        repository.deleteById(id);
    }
}
