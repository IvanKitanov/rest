package toDoList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController


public class ToDoListController {
    @Autowired
    private ToDoListService service;
    @RequestMapping(value = "/tasks",method = RequestMethod.GET)
    public List<ToDoList> getAllTasks(){
        return service.getAll();
    }
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ToDoList getTaskById(@RequestParam(value="number") long id){
        return service.singleTask(id);
    }
    @RequestMapping(value = "/tasks",method = RequestMethod.POST)
    public void addTask(@RequestParam(value="title") String title){
        service.addNewTask(new ToDoList(title));
    }
    @RequestMapping(value= "/tasks",method = RequestMethod.PUT)
    public void updateTask(@RequestParam(value="id") long id,@RequestParam(value="title") String title ){
        service.changeTask(new ToDoList(title),id);
    }
    @RequestMapping(value = "/tasks",method = RequestMethod.DELETE)
    public void deleteTask(@RequestParam(value="id") long id){
        service.deleteEmployee(id);
    }
}